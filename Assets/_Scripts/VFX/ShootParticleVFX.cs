﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootParticleVFX : MonoBehaviour
{
    [SerializeField] private ParticleSystem targetSystem;



    private void OnShoot()
    {
        targetSystem.Play();
    }

    private void OnEnable()
    {
        ShootBehaviour.onShootEvent += OnShoot;
    }

    private void OnDisable()
    {
        ShootBehaviour.onShootEvent -= OnShoot;
    }
}
