﻿using Project.Inputs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShakeVFX : MonoBehaviour
{
    [SerializeField] private float horizontalAmp;
    [SerializeField] private float verticalAmp;
    [SerializeField] private float duration;
    [SerializeField, Range(0, 1f)] private float lerpAmp;

    private Coroutine coroutine_Shake;
    private Transform cachedTransform;



    private void OnShotVFX()
    {
        if (coroutine_Shake != null)
            StopCoroutine(coroutine_Shake);

        coroutine_Shake = StartCoroutine(CoroutineShake(horizontalAmp, verticalAmp, duration, lerpAmp));
    }

    private void OnEnable()
    {
        cachedTransform = transform;
        ShootBehaviour.onShootEvent += OnShotVFX;
    }

    private void OnDisable()
    {
        ShootBehaviour.onShootEvent -= OnShotVFX;
    }

    private IEnumerator CoroutineShake(float horizontalAmp, float verticalAmp, float duration, float lerpAmp)
    {
        float timer = 0;
        float halfDuration = duration / 2f;

        while(timer < halfDuration)
        {
            timer += Time.deltaTime;

            float randomX = Random.Range(-horizontalAmp, horizontalAmp);
            float randomY = Random.Range(-verticalAmp, verticalAmp);

            cachedTransform.localPosition = Vector3.Lerp(cachedTransform.localPosition, new Vector3(randomX, randomY), lerpAmp);

            yield return null;
        }

        timer = 0;

        while (timer < halfDuration)
        {
            timer += Time.deltaTime;

            cachedTransform.localPosition = Vector3.Lerp(cachedTransform.localPosition, Vector3.zero, lerpAmp);

            yield return null;
        }

        coroutine_Shake = null;
    }
}