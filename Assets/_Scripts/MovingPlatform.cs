﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : Block 
{
    public Vector2 destinationPosition;
    public float platformSpeed;
    public float platformMoveTime;
    Vector2 initialPosition;
    Coroutine moveCoroutine;

    protected override void Start()
    {
        base.Start();
        initialPosition = transform.position;

		moveCoroutine = StartCoroutine(MoveToDestination());
	}

    protected override void OnDrawGizmos()
    {
        if (!Application.isPlaying)
        {
            base.OnDrawGizmos();
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, (Vector2)transform.position + destinationPosition);
            Gizmos.DrawWireCube((Vector2)transform.position + destinationPosition, Vector2.one);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.transform.SetParent(transform);
        }
	}
	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player")
		{
            if(other.transform.parent == transform)
			    other.transform.SetParent(null);
		}
	}

    IEnumerator MoveToDestination()
    {
        for (int i = 0; i < 60 * platformMoveTime; i++)
        {
            transform.position = Vector2.MoveTowards(transform.position, initialPosition + destinationPosition, platformSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        moveCoroutine = StartCoroutine(MoveBackToInitialPosition());
    }

    IEnumerator MoveBackToInitialPosition()
	{
		for (int i = 0; i < 60 * platformMoveTime; i++)
		{
			transform.position = Vector2.MoveTowards(transform.position, initialPosition, platformSpeed * Time.deltaTime);
			yield return new WaitForEndOfFrame();
		}

		moveCoroutine = StartCoroutine(MoveToDestination());
    }
}