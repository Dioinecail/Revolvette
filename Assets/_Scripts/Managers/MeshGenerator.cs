﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator : MonoBehaviour 
{
    private Mesh mesh;
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    private MeshCollider levelCollider;

    private List<Vector3> newVertices;
    private List<Vector2> newUV;
    private List<int> newTriangles;

    int faces;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshFilter = GetComponent<MeshFilter>();
        mesh = GetComponent<MeshFilter>().mesh;
        levelCollider = GetComponent<MeshCollider>();

        newVertices = new List<Vector3>();
        newUV = new List<Vector2>();
        newTriangles = new List<int>();
        faces = 0;
    }

    public void GenerateMesh(GridHolder[] grids)
    {
        for (int i = 0; i < grids.Length; i++)
        {
            AddGrid(grids[i]);
        }

		mesh.Clear();
		mesh.vertices = newVertices.ToArray();
		mesh.uv = newUV.ToArray();
		mesh.triangles = newTriangles.ToArray();
		mesh.RecalculateNormals();

        levelCollider.sharedMesh = null;
        levelCollider.sharedMesh = mesh;

		newVertices.Clear();
		newUV.Clear();
		newTriangles.Clear();
    }	
    public void AddGrid(GridHolder grid)
    {
        for (int x = 0; x < grid.grid.GetLength(0); x++)
        {
            for (int y = 0; y < grid.grid.GetLength(1); y++)
            {
                if(grid.grid[x,y].tileType == TileType.Block)
                {
					GenerateBlock(grid, x, y);
				} else if(grid.grid[x, y].tileType == TileType.Tree) {
					// generate a block
					GenerateBlock(grid, x, y);
					// spawn a tree
					SpawnPrefab(GameManager.Instance.currentLevel.treePrefab, grid.offset + new Vector2(x, y + 1));
				}
            }
        }
    }
	void SpawnPrefab(GameObject prefab, Vector2 position) {
		Instantiate(prefab, (Vector3)(position + new Vector2(0.5f, 0)) + new Vector3(0, 0, 1), Quaternion.identity);
	}
	void GenerateBlock(GridHolder grid, int x, int y) {
		// front
		Vector3 position = new Vector2(grid.grid[x, y].X + grid.offset.x, grid.grid[x, y].Y + grid.offset.y);

		newVertices.Add(position + new Vector3(0, 1));
		newVertices.Add(position + new Vector3(1, 1));
		newVertices.Add(position + new Vector3(1, 0));
		newVertices.Add(position + new Vector3(0, 0));

		newTriangles.Add(faces * 4);
		newTriangles.Add(faces * 4 + 1);
		newTriangles.Add(faces * 4 + 2);

		newTriangles.Add(faces * 4);
		newTriangles.Add(faces * 4 + 2);
		newTriangles.Add(faces * 4 + 3);

		faces++;

		newUV.Add(new Vector2(0, 0));
		newUV.Add(new Vector2(0, 1));
		newUV.Add(new Vector2(1, 1));
		newUV.Add(new Vector2(1, 0));

		// top
		if((grid.IsInsideGrid(x, y + 1) && grid.grid[x, y + 1].tileType == TileType.Empty) || !(grid.IsInsideGrid(x, y + 1))) {
			newVertices.Add(position + new Vector3(0, 1, 1));
			newVertices.Add(position + new Vector3(1, 1, 1));
			newVertices.Add(position + new Vector3(1, 1, 0));
			newVertices.Add(position + new Vector3(0, 1, 0));

			newTriangles.Add(faces * 4);
			newTriangles.Add(faces * 4 + 1);
			newTriangles.Add(faces * 4 + 2);
			
			newTriangles.Add(faces * 4);
			newTriangles.Add(faces * 4 + 2);
			newTriangles.Add(faces * 4 + 3);
			faces++;

			newUV.Add(new Vector2(0, 0));
			newUV.Add(new Vector2(0, 1));
			newUV.Add(new Vector2(1, 1));
			newUV.Add(new Vector2(1, 0));
		}
		// bottom
		if ((grid.IsInsideGrid(x, y - 1) && grid.grid[x, y -1].tileType == TileType.Empty) || !grid.IsInsideGrid(x, y - 1))	{
			newVertices.Add(position + new Vector3(0, 0, 0));
			newVertices.Add(position + new Vector3(1, 0, 0));
			newVertices.Add(position + new Vector3(1, 0, 1));
			newVertices.Add(position + new Vector3(0, 0, 1));

			newTriangles.Add(faces * 4);
			newTriangles.Add(faces * 4 + 1);
			newTriangles.Add(faces * 4 + 2);

			newTriangles.Add(faces * 4);
			newTriangles.Add(faces * 4 + 2);
			newTriangles.Add(faces * 4 + 3);
			faces++;

			newUV.Add(new Vector2(0, 0));
			newUV.Add(new Vector2(0, 1));
			newUV.Add(new Vector2(1, 1));
			newUV.Add(new Vector2(1, 0));
		}
		// west
		if ((grid.IsInsideGrid(x + 1, y) && grid.grid[x + 1, y].tileType == TileType.Empty) || !grid.IsInsideGrid(x + 1, y)) {
			newVertices.Add(position + new Vector3(1, 1, 0));
			newVertices.Add(position + new Vector3(1, 1, 1));
			newVertices.Add(position + new Vector3(1, 0, 1));
			newVertices.Add(position + new Vector3(1, 0, 0));

			newTriangles.Add(faces * 4);
			newTriangles.Add(faces * 4 + 1);
			newTriangles.Add(faces * 4 + 2);

			newTriangles.Add(faces * 4);
			newTriangles.Add(faces * 4 + 2);
			newTriangles.Add(faces * 4 + 3);
			faces++;

			newUV.Add(new Vector2(0, 0));
			newUV.Add(new Vector2(0, 1));
			newUV.Add(new Vector2(1, 1));
			newUV.Add(new Vector2(1, 0));
		}
		// east
		if ((grid.IsInsideGrid(x - 1, y) && grid.grid[x - 1, y].tileType == TileType.Empty) || !grid.IsInsideGrid(x - 1, y)) {
			newVertices.Add(position + new Vector3(0, 1, 1));
			newVertices.Add(position + new Vector3(0, 1, 0));
			newVertices.Add(position + new Vector3(0, 0, 0));
			newVertices.Add(position + new Vector3(0, 0, 1));

			newTriangles.Add(faces * 4);
			newTriangles.Add(faces * 4 + 1);
			newTriangles.Add(faces * 4 + 2);

			newTriangles.Add(faces * 4);
			newTriangles.Add(faces * 4 + 2);
			newTriangles.Add(faces * 4 + 3);
			faces++;

			newUV.Add(new Vector2(0, 0));
			newUV.Add(new Vector2(0, 1));
			newUV.Add(new Vector2(1, 1));
			newUV.Add(new Vector2(1, 0));
		}
	}
}