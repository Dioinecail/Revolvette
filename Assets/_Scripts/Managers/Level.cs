﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour 
{
    public Transform[] SpawnPoints;

    public GameObject[] EnemyPrefabs;

    public GameObject treePrefab;

    public GameObject player;

    public float SpawnDelay;

    public int MaxEnemyCount;
    public static int currentEnemyCount;

    public GridHolder[] grids;

    public GridTile[,] grid;

    public int sizeX, sizeY;

    [Range(0, 100)]
    public float fillPercent;

    [Range(0, 100)]
    public float treePrecent;

    public int maxNeighbourCount;
    public int minNeighbourCount;

    public TileType firstRuleTile;
    public TileType secondRuleTile;

    public int processIterations;

    private MeshGenerator meshGen;

    private void Start()
    {
        GameManager.Instance.currentLevel = this;

        GameManager.Instance.StartSpawning();

        meshGen = GetComponent<MeshGenerator>();
        StartCoroutine(BuildLevel());
    }

    private void Update()
    {
    }

    public void SpawnEnemy()
    {
        if (currentEnemyCount < MaxEnemyCount)
        {
            int spawnNumber = Random.Range(0, SpawnPoints.Length);
            int enemyNumber = Random.Range(0, EnemyPrefabs.Length);

            Instantiate(EnemyPrefabs[enemyNumber], SpawnPoints[spawnNumber].position, Quaternion.identity);
            currentEnemyCount++;
        }
    }


    IEnumerator GenerateGrids()
    {
		int randomNum;

        for (int i = 0; i < grids.Length; i++)
        {
            grids[i].grid = new GridTile[grids[i].sizeX, grids[i].sizeY];
            grids[i].spawnerPlaced = false;

            for (int x = 0; x < grids[i].sizeX; x++)
            {
                for (int y = 0; y < grids[i].sizeY; y++)
                {
					randomNum = Random.Range(0, 101);

					grids[i].grid[x, y] = new GridTile(x, y, randomNum < fillPercent ? TileType.Block : TileType.Empty);
				}
            }
			yield return new WaitForEndOfFrame();
		}
    }

    IEnumerator ProcessGrids()
    {
        for (int i = 0; i < grids.Length; i++)
        {
            for (int x = 0; x < grids[i].sizeX; x++)
            {
                for (int y = 0; y < grids[i].sizeY; y++)
                {

                    if (grids[i].grid[x, y].tileType == (TileType.Spawner))
                        continue;

                    if (grids[i].offset.y == 0 && y == 0)
                    {
                        grids[i].grid[x, y].tileType = TileType.Block;
                        continue;
                    }

                    if(y == grids[i].sizeY - 1) {
                        // spawn a tree at a random pace
                        if(Random.Range(0, 101) < treePrecent) {
                            grids[i].grid[x,y].tileType = TileType.Tree;
                        }
                    }

                    int neighbours = GetNeighbourBlocksCount(x, y,grids[i].grid);

                    if (neighbours >= grids[i].UpperRule)
                    {
                        grids[i].grid[x, y].tileType = grids[i].upperTile;
                    }
                    else if (neighbours <= grids[i].LowerRule)
                    {
                        grids[i].grid[x, y].tileType = grids[i].lowerTile;
                    }

                    if (CheckFourSidesForEmptySpaces(x, y, grids[i].grid))
                    {
                        grids[i].grid[x, y].tileType = TileType.Empty;
                    }

                }
            }
            // add spawner to a random spot above 
            if(grids[i].spawner && !grids[i].spawnerPlaced)
            {
                int randomX = 0;
                do
                {
                    randomX = Random.Range(0, grids[i].sizeX);
                }
                while (grids[i].grid[randomX, grids[i].sizeY - 2].tileType == TileType.Empty);

				grids[i].grid[randomX, grids[i].sizeY - 1].tileType = TileType.Spawner;
                grids[i].spawnerPlaced = true;
            }

            yield return new WaitForEndOfFrame();
		}
    }

    IEnumerator BuildLevelOnGrids()
    {
        //foreach (var grd in grids)
        //{
        //    int num = 0;
        //    for (int x = 0; x < grd.sizeX; x++)
        //    {
        //        for (int y = 0; y < grd.sizeY; y++)
        //        {
        //            switch (grd.grid[x,y].tileType)
        //            {
        //                case TileType.Empty:
        //                    break;
        //                case TileType.Block:
        //                    GameObject block = Instantiate(blockPrefab, new Vector2(x, y) + grd.offset, Quaternion.identity, transform);
        //                    break;
        //                case TileType.Spawner:
        //                    break;

        //                default:
        //                    break;
        //            }
        //            num++;

        //            if(num % 50 == 0)
        //                yield return new WaitForEndOfFrame();
        //        }
        //    }
        //}

        meshGen.GenerateMesh(grids);
        yield return null;
    }

    IEnumerator GenerateFirstGrid()
    {
        grid = new GridTile[sizeX, sizeY];

        int randomNum;

        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                randomNum = Random.Range(0, 101);

                grid[x, y] = new GridTile(x, y, randomNum < fillPercent ? TileType.Block : TileType.Empty);
            }
        }
        yield return new WaitForEndOfFrame();
    }

    IEnumerator ProcessLevel()
    {
		for (int x = 0; x < sizeX; x++)
		{
			for (int y = 0; y < sizeY; y++)
			{
                int neighbours = GetNeighbourBlocksCount(x, y);

                if(neighbours >= maxNeighbourCount)
                {
                    grid[x, y].tileType = firstRuleTile;
                }
                //else if (neighbours > minNeighbourCount && neighbours < maxNeighbourCount)
                //{
                //    grid[x, y].tileType = TileType.Block;
                //}
                else if (neighbours <= minNeighbourCount)
                {
                    grid[x, y].tileType = secondRuleTile;
                }

                if (CheckFourSidesForEmptySpaces(x,y))
                {
                    grid[x, y].tileType = TileType.Empty;
                }
			}
		}

        yield return new WaitForEndOfFrame();
    }

    int GetNeighbourBlocksCount(int startX, int startY)
    {
        int neighbourCount = 0;

        for (int x = startX - 1; x <= startX + 1; x++)
        {
            for (int y = startY - 1; y <= startY + 1; y++)
            {
                if(IsInsideGrid(x,y))
                {
                    if (grid[x, y].tileType == TileType.Block)
                        neighbourCount++;
                }
            }
        }

        return neighbourCount;
	}

	int GetNeighbourBlocksCount(int startX, int startY, GridTile[,] grd)
	{
		int neighbourCount = 0;

		for (int x = startX - 1; x <= startX + 1; x++)
		{
			for (int y = startY - 1; y <= startY + 1; y++)
			{
				if (IsInsideGrid(x, y,grd))
				{
					if (grd[x, y].tileType == TileType.Block)
						neighbourCount++;
				}
			}
		}

		return neighbourCount;
	}

    bool CheckFourSidesForEmptySpaces(int x, int y)
    {
        int emptyVert = 0;
        int emptyHor = 0;

        if(IsInsideGrid(x - 1, y))
        {
            if (grid[x - 1, y].tileType == TileType.Empty)
                emptyHor++;
		}
		if (IsInsideGrid(x + 1, y))
		{
			if (grid[x + 1, y].tileType == TileType.Empty)
				emptyHor++;
		}
		if (IsInsideGrid(x, y - 1))
		{
			if (grid[x, y - 1].tileType == TileType.Empty)
				emptyVert++;
		}
		if (IsInsideGrid(x, y + 1))
		{
			if (grid[x, y + 1].tileType == TileType.Empty)
				emptyVert++;
		}

        return emptyHor == 2 || emptyVert == 2;
	}

	bool CheckFourSidesForEmptySpaces(int x, int y, GridTile[,] grd)
	{
		int emptyVert = 0;
		int emptyHor = 0;

		if (IsInsideGrid(x - 1, y,grd))
		{
			if (grd[x - 1, y].tileType == TileType.Empty)
				emptyHor++;
		}
		if (IsInsideGrid(x + 1, y,grd))
		{
			if (grd[x + 1, y].tileType == TileType.Empty)
				emptyHor++;
		}
		if (IsInsideGrid(x, y - 1,grd))
		{
			if (grd[x, y - 1].tileType == TileType.Empty)
				emptyVert++;
		}
		if (IsInsideGrid(x, y + 1,grd))
		{
			if (grd[x, y + 1].tileType == TileType.Empty)
				emptyVert++;
		}

		return emptyHor == 2 || emptyVert == 2;
	}

    bool IsInsideGrid(int x, int y)
    {
        return x >= 0 && x < sizeX && y >= 0 && y < sizeY;
	}

	bool IsInsideGrid(int x, int y, GridTile[,] grd)
	{
		return x >= 0 && x < grd.GetLength(0) && y >= 0 && y < grd.GetLength(1);
	}

    IEnumerator BuildLevel()
    {
        player.SetActive(false);
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }

        yield return GenerateGrids();

        for (int i = 0; i < processIterations; i++)
        {
            yield return ProcessGrids();

			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
        }

        yield return BuildLevelOnGrids();
		player.SetActive(true);
	}

    void OnDrawGizmos()
    {
        if (grids != null)
        {
            foreach (var grd in grids)
            {
                if(grd.grid != null)
                {
					//foreach (var tile in grd.grid)
					//{
						
					//	Color tileColor = Color.white;
						
					//	switch (tile.tileType)
					//	{
					//		case TileType.Block:
					//			tileColor = Color.black;
					//			break;
     //                       case TileType.Spawner:
     //                           tileColor = Color.red;
     //                           break;
					//		default:
					//			break;
					//	}
					//	Gizmos.color = tileColor;
					//	Gizmos.DrawCube(new Vector3(tile.X + grd.offset.x, tile.Y + grd.offset.y), Vector2.one);
					//}
                }
                else 
                {
					for (int x = 0; x < grd.sizeX; x++)
					{
						for (int y = 0; y < grd.sizeY; y++)
						{
							Gizmos.color = Color.magenta;

                            float colorX = grd.sizeX / 50f;
                            float colorY = grd.sizeY / 50f;

                            Gizmos.color = new Color(colorX, colorY, 1, 1);

                            if(grd.spawner && x == 0)
                            {
                                Gizmos.color = Color.red;
                            }
                            if (grd.offset.y == 0 && y == 0)
                            {
                                Gizmos.color = Color.cyan;
                            }

							Gizmos.DrawWireCube(new Vector3(grd.offset.x + x, grd.offset.y + y), Vector2.one);
						}
					}
                }
            }
        }
    }
}
[System.Serializable]
public struct GridHolder
{
    public Vector2 offset;
    public int sizeX, sizeY;
    public GridTile[,] grid;

	public bool spawner;
    public bool spawnerPlaced;

    public int UpperRule;
    public int LowerRule;

    public TileType upperTile;
    public TileType lowerTile;

    public GridHolder(Vector2 startPosition, int xSize, int ySize)
    {
        offset = startPosition;
        sizeX = xSize;
        sizeY = ySize;
        grid = null;
        spawner = false;
        spawnerPlaced = false;
        UpperRule = 5;
        LowerRule = 3;

        upperTile = TileType.Block;
        lowerTile = TileType.Empty;
    }

	public bool IsInsideGrid(int x, int y)
	{
		return x >= 0 && x < sizeX && y >= 0 && y < sizeY;
	}
}