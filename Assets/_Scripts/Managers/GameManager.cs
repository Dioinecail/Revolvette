﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour 
{
    public static GameManager Instance;
    public BonusManager bonusManager;

    public Level currentLevel;


    public int Coins;

    Coroutine spawnCoroutine;

    private void Awake()
    {
        Instance = this;
        bonusManager.Initialize();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            RestartGame();
    }

    public void SpawnEnemy()
    {
        if(currentLevel != null)
        {
            currentLevel.SpawnEnemy();
        }
    }


    public void RestartGame()
    {
		SceneManager.LoadScene(0);
	}

    public void StartSpawning()
    {
        spawnCoroutine = StartCoroutine(SpawnTimer());
    }

    IEnumerator SpawnTimer()
    {
        yield return new WaitForSeconds(currentLevel.SpawnDelay);

        currentLevel.SpawnEnemy();

        yield return SpawnTimer();
    }
}
