﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BonusManager : ScriptableObject
{
    public static BonusManager Instance;

	public GameObject CoinPrefab;
	public GameObject AmmoPrefab;
	public GameObject HealthPrefab;

    [SerializeField]
    int BaseAmmoSpawnChance;
	[SerializeField]
    int BaseHealthSpawnChance;
	[SerializeField]
    int BaseCoinAverageAmount;

    public int AmmoSecondsDecay = 100;
    public int HealthSecondsDecay = 100;
    public int MinimumAmmoSpawnChance;

    public void Initialize()
    {
        Instance = this;
    }

    public int GetCoinAmount()
    {
        return BaseCoinAverageAmount + Random.Range(-BaseCoinAverageAmount / 2, BaseCoinAverageAmount / 2);
    }

    public int GetAmmoChance()
    {
        return (int)Mathf.Clamp((BaseAmmoSpawnChance - (BaseAmmoSpawnChance * (Time.time / 100))), MinimumAmmoSpawnChance, 100);
    }
    public int GetHealthChance()
    {
        return (int)(BaseHealthSpawnChance - (BaseHealthSpawnChance * (Time.time / 100)));
    }
}
