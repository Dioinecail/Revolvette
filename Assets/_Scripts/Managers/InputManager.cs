﻿namespace Project.Inputs
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class InputManager : MonoBehaviour
    {
        public const string HORIZONTAL_INPUT_STRING = "Horizontal";
        public const string VERTICAL_INPUT_STRING = "Vertical";

        public static event Action<Vector2> directionInputEvent;
        public static event Action jumpInputEvent;
        public static event Action jumpOffInputEvent;
        public static event Action shootInputEvent;
        public static event Action<Vector2> aimInputEvent;

        private Camera cachedCamera;



        private void Update()
        {
            ProcessDirectionalInput();
            ProcessJumpInput();
            ProcessShootInput();
            ProcessAimInput();
        }

        private void ProcessAimInput()
        {
            Vector2 cursorPosition = cachedCamera.ScreenToWorldPoint(Input.mousePosition);

            aimInputEvent?.Invoke(cursorPosition);
        }

        private void ProcessShootInput()
        {
            if (Input.GetMouseButtonDown(0))
                shootInputEvent?.Invoke();
        }

        private void ProcessJumpInput()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                jumpInputEvent?.Invoke();
            if (Input.GetKeyUp(KeyCode.Space))
                jumpOffInputEvent?.Invoke();
        }

        private void ProcessDirectionalInput()
        {
            float xInput = Input.GetAxis(HORIZONTAL_INPUT_STRING);
            float yInput = Input.GetAxis(VERTICAL_INPUT_STRING);

            directionInputEvent?.Invoke(new Vector2(xInput, yInput));
        }

        private void OnEnable()
        {
            cachedCamera = Camera.main;
        }
    }
}