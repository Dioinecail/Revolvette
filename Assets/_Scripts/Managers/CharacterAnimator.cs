﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimator : MonoBehaviour 
{
    public Animator Anima;

    public void SetVelocity(float x)
    {
        Anima.SetFloat("Velocity", Mathf.Abs(x));
    }

    public void SetOnGround(bool state)
    {
        Anima.SetBool("OnGround", state);
    }

    public void Jump()
    {
        Anima.Play("Jump");
    }

    public void ShootJump()
    {
        Anima.Play("ShootJump", 0, 0);
    }
}
