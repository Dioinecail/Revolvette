﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public struct GridTile : IComparable<GridTile>
{
    public int X;
    public int Y;

    public TileType tileType;

    public GridTile(int _x, int _y, TileType type)
    {
        X = _x;
        Y = _y;
        tileType = type;
    }
    public GridTile(Vector2 pos, TileType type)
    {
        int x = Mathf.RoundToInt(pos.x);
        int y = Mathf.RoundToInt(pos.y);

        X = x;
        Y = y;
        tileType = type;
    }

    public int CompareTo(GridTile other)
    {
        if (other.tileType == this.tileType)
            return 1;
        else 
            return 0;
    }
}

public enum TileType
{
    Empty,
    Block,
    Spawner,
    TrapBlock,
    Tree
}