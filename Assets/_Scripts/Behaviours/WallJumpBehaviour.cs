﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallJumpBehaviour : AbstractBehaviour {
    MoveBehaviour moveBehaviour;
    public float JumpForce;

    protected override void Awake()
    {
        base.Awake();
        moveBehaviour = GetComponent<MoveBehaviour>();
    }

	void Update () 
    {
        if(collision.OnWall && !collision.OnGround && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.K)))
        {
            Jump((int)moveBehaviour.direction);
        }
	}

    void Jump(int direction)
    {
        rBody.velocity = new Vector2(-direction * JumpForce * 0.7f, JumpForce);
    }
}
