﻿using Project.Inputs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void LandEvent ();

public class CollisionBehaviour : MonoBehaviour 
{
    public const string GROUND_COLLISION_TAG = "Ground";
    public static event LandEvent onLandEvent;

    public float CollisionRadius;
    public Vector2 BottomPosition;
    public Vector2 RightPosition;

    public LayerMask collisionLayer;

    public bool OnGround { get; private set; }
    public bool OnWall { get; private set; }

    public CharacterAnimator anima;

    private Collider2D[] collisionColliders = new Collider2D[1];
    private bool canCheckGround = true;
    private Coroutine coroutine_DelayGroundCheck;
    


    void Update()
    {
        collisionColliders[0] = null;

        if (!OnGround && !canCheckGround)
        {

        }
        else
        {
            bool onGroundLastFrame = OnGround;

            Physics2D.OverlapCircleNonAlloc((Vector2)transform.position + BottomPosition, CollisionRadius, collisionColliders, collisionLayer);

            if (collisionColliders[0] != null)
                OnGround = collisionColliders[0].tag == GROUND_COLLISION_TAG;
            else
                OnGround = false;

            if (!onGroundLastFrame && OnGround)
            {
                if (onLandEvent != null)
                {
                    onLandEvent();
                }
            }
        }

        Physics2D.OverlapCircleNonAlloc((Vector2)transform.position + RightPosition, CollisionRadius, collisionColliders, collisionLayer);

        bool rightWall = false;

        if(collisionColliders[0] != null) 
            rightWall = collisionColliders[0].tag == GROUND_COLLISION_TAG;

		Physics2D.OverlapCircleNonAlloc((Vector2)transform.position + new Vector2(-RightPosition.x, RightPosition.y), CollisionRadius, collisionColliders, collisionLayer);

        bool leftWall = false; 

        if(collisionColliders[0] != null) 
            leftWall = collisionColliders[0].tag == GROUND_COLLISION_TAG;

        OnWall = (rightWall || leftWall);

        anima.SetOnGround(OnGround);
    }

    private void DelayGroundCheck()
    {
        if(OnGround && coroutine_DelayGroundCheck == null)
        {
            coroutine_DelayGroundCheck = StartCoroutine(CoroutineDelayGroundCheck());
        }
    }

    private IEnumerator CoroutineDelayGroundCheck()
    {
        canCheckGround = false;

        yield return new WaitForSeconds(0.1f);

        canCheckGround = true;
    }

    private void OnEnable()
    {
        InputManager.jumpInputEvent += DelayGroundCheck;
    }

    private void OnDisable()
    {
        InputManager.jumpInputEvent -= DelayGroundCheck;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
		Gizmos.DrawWireSphere((Vector2)transform.position + BottomPosition, CollisionRadius);
		Gizmos.DrawWireSphere((Vector2)transform.position + RightPosition, CollisionRadius);
		Gizmos.DrawWireSphere((Vector2)transform.position + new Vector2(-RightPosition.x, RightPosition.y), CollisionRadius);
    }
}