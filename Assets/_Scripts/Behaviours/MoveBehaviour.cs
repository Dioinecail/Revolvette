﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBehaviour : AbstractBehaviour 
{
    public float Speed;
    public Direction direction;

    float localScale;

    public CharacterAnimator anima;

    public Transform BodyAnchor;

    private void Start()
    {
        localScale = BodyAnchor.localScale.x;
    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal");

        if (x > 0)
            direction = Direction.Right;
        else if (x < 0)
            direction = Direction.Left;

        BodyAnchor.localScale = new Vector3((int)direction * localScale, BodyAnchor.localScale.y, BodyAnchor.localScale.z);

		Move(x);
    }

    public void Move(float dir)
	{
		if (dir > 0)
			direction = Direction.Right;
		else if (dir < 0)
			direction = Direction.Left;
        
        rBody.velocity = Vector2.Lerp(rBody.velocity, new Vector2(dir * Speed, rBody.velocity.y), 0.225f);

        anima.SetVelocity(rBody.velocity.x);
    }
}

public enum Direction
{
    Left = -1,
    Right = 1
}