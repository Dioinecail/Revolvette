﻿using UnityEngine;

[CreateAssetMenu(fileName = "PB_", menuName = "ProjectileBehaviours/New Multi")]
public class MultiProjectileBehaviour : ProjectileBehaviourBase
{
    [SerializeField] private float spreadAngle;
    [SerializeField] private int projectileCount;
    [SerializeField] private float speedVariance;



    public override void Execute(Vector2 origin, Vector2 direction, Quaternion rotation)
    {
        // spawn one projectile right in the middle
        Projectile p1 = GameobjectPoolSystem.Instantiate(projectilePrefab, origin, rotation).GetComponent<Projectile>();
        p1.InitialDirection = direction;
        p1.Speed = projectileSpeed;
        p1.Damage = projectileDamage;
        p1.CollisionRadius = projectileCollision;

        for (int i = 0; i < projectileCount - 1; i++)
        {
            float randomAngle = Random.Range(-spreadAngle, spreadAngle);

            Vector2 randomDirection = Quaternion.Euler(0, 0, randomAngle) * direction;
            float randomSpeed = Random.Range(-speedVariance, speedVariance);

            Projectile p2 = GameobjectPoolSystem.Instantiate(projectilePrefab, origin, rotation).GetComponent<Projectile>();
            p2.InitialDirection = randomDirection;
            p2.Speed = projectileSpeed + randomSpeed;
            p2.Damage = projectileDamage;
            p2.CollisionRadius = projectileCollision;
        }
    }
}