﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PB_", menuName = "ProjectileBehaviours/New Single")]
public class SingleProjectileBehaviour : ProjectileBehaviourBase
{
    public override void Execute(Vector2 origin, Vector2 direction, Quaternion rotation)
    {
        GameObject projectile = GameobjectPoolSystem.Instantiate(projectilePrefab, origin, rotation, true);
        Projectile pScript = projectile.GetComponent<Projectile>();
        pScript.InitialDirection = direction;
        pScript.Damage = projectileDamage;
        pScript.Speed = projectileSpeed;
        pScript.CollisionRadius = projectileCollision;
    }
}