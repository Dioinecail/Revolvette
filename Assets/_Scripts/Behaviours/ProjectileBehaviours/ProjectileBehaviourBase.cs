﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProjectileBehaviourBase : ScriptableObject
{
    [SerializeField] protected GameObject projectilePrefab;
    [SerializeField] protected float projectileSpeed;
    [SerializeField] protected float projectileCollision;
    [SerializeField] protected int projectileDamage;

    public abstract void Execute(Vector2 origin, Vector2 direction, Quaternion rotation);
}
