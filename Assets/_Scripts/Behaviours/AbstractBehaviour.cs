﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractBehaviour : MonoBehaviour 
{
    protected Rigidbody2D rBody;
    protected CollisionBehaviour collision;

    protected virtual void Awake()
    {
        rBody = GetComponent<Rigidbody2D>();
        collision = GetComponent<CollisionBehaviour>();
    }
}
