﻿using Project.Inputs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBehaviour : AbstractBehaviour 
{
    public float jumpForce;
    public float furtherJumpMultiplier;

    public float onJumpOffDamping;

    public MoveBehaviour moveBehaviour;
    public CharacterAnimator anima;

    private bool hasShotJumped;
    private bool canShotJump = true;

    [Header("EDITOR")]
    public int debugIterations = 20;
    [Range(0.01f, 0.4f)]
    public float debugTime = 0.01f;




    private void OnJump()
    {
        if (!collision.OnGround)
        {
			return;
        }
        
        rBody.velocity = new Vector2(rBody.velocity.x, jumpForce);
        anima.Jump();
    }

    private void OnJumpOff()
    {
        if (hasShotJumped)
            return;

        Vector2 currentVelocity = rBody.velocity;

        if (currentVelocity.y > 0)
            currentVelocity.y /= onJumpOffDamping;

        rBody.velocity = currentVelocity;
    }

    private void OnShootJump()
    {
        if (!canShotJump || collision.OnGround)
            return;

        hasShotJumped = true;

        rBody.velocity = new Vector2(rBody.velocity.x, jumpForce * furtherJumpMultiplier);
        anima.ShootJump();
	}

    private void OnLanded()
    {
        hasShotJumped = false;
    }

    private void OnReloadStarted()
    {
        canShotJump = false;
    }

    private void OnReloadFinished()
    {
        canShotJump = true;
    }

    private void OnEnable()
    {
        InputManager.jumpInputEvent += OnJump;
        InputManager.jumpOffInputEvent += OnJumpOff;
        ShootBehaviour.onShootEvent += OnShootJump;
        ShootBehaviour.onReloadStarted += OnReloadStarted;
        ShootBehaviour.onReloadFinished += OnReloadFinished;
        CollisionBehaviour.onLandEvent += OnLanded;
    }

    private void OnDisable()
    {
        InputManager.jumpInputEvent -= OnJump;
        InputManager.jumpOffInputEvent -= OnJumpOff;
        ShootBehaviour.onShootEvent -= OnShootJump;
        ShootBehaviour.onReloadStarted -= OnReloadStarted;
        ShootBehaviour.onReloadFinished -= OnReloadFinished;
        CollisionBehaviour.onLandEvent -= OnLanded;
    }

    #region EDITOR

    private void OnDrawGizmos()
    {
        DebugJumpHeight();
    }

    private void DebugJumpHeight()
    {
        Vector3 jumpPosition = transform.position;

        Gizmos.color = Color.green;

        float peakTime = PeakHeightTime(jumpForce, Physics2D.gravity.y);
        float peakHeight = HeightByTime(jumpPosition.y, jumpForce, Physics2D.gravity.y, peakTime);
        float peakPos = jumpPosition.x + moveBehaviour.Speed * peakTime;
        float peakPosLeft = jumpPosition.x - moveBehaviour.Speed * peakTime;

        Gizmos.DrawWireSphere(new Vector3(peakPos, peakHeight), 0.1f);
        Gizmos.DrawWireSphere(new Vector3(peakPosLeft, peakHeight), 0.1f);

        for (int i = 0; i < debugIterations; i++)
        {
            float lastHeight = HeightByTime(jumpPosition.y, jumpForce, Physics2D.gravity.y, i * debugTime);
            float height = HeightByTime(jumpPosition.y, jumpForce, Physics2D.gravity.y, (i + 1) * debugTime);

            float lastPos = jumpPosition.x + (i * debugTime * moveBehaviour.Speed);
            float pos = jumpPosition.x + ((i+ 1) * debugTime * moveBehaviour.Speed);

            Vector3 p0 = new Vector3(lastPos, lastHeight);
            Vector3 p1 = new Vector3(pos, height);

            Gizmos.DrawLine(p0, p1);
            MirrorDebugLine(jumpPosition, p0, p1);

            Vector3 flatLastPos = p0;
            flatLastPos.x = jumpPosition.x;
            Vector3 flatCurrentPosition = p1;
            flatCurrentPosition.x = flatLastPos.x;
            Gizmos.DrawLine(flatLastPos, flatCurrentPosition);
        }

        Gizmos.color = Color.cyan;

        for (int i = 0; i < 6; i++)
        {
            float lastPeakPos = peakPos;
            float lastPeakPosLeft = peakPosLeft;
            float lastPeakHeight = peakHeight;

            peakTime = PeakHeightTime(jumpForce * furtherJumpMultiplier, Physics2D.gravity.y);
            peakHeight = HeightByTime(peakHeight, jumpForce * furtherJumpMultiplier, Physics2D.gravity.y, peakTime);

            peakPos = peakPos + moveBehaviour.Speed * peakTime;
            peakPosLeft = peakPosLeft - moveBehaviour.Speed * peakTime;

            Gizmos.DrawWireSphere(new Vector3(peakPos, peakHeight), 0.1f);
            Gizmos.DrawWireSphere(new Vector3(peakPosLeft, peakHeight), 0.1f);

            Gizmos.DrawLine(new Vector3(lastPeakPos, lastPeakHeight), new Vector3(peakPos, peakHeight));
            Gizmos.DrawLine(new Vector3(lastPeakPosLeft, lastPeakHeight), new Vector3(peakPosLeft, peakHeight));
        }
    }

    private void MirrorDebugLine(Vector3 origin, Vector3 start, Vector3 end)
    {
        Vector3 transformStart = start - origin;
        transformStart.x *= -1;
        transformStart = origin + transformStart;

        Vector3 transformEnd = end - origin;
        transformEnd.x *= -1;
        transformEnd = origin + transformEnd;

        Gizmos.DrawLine(transformStart, transformEnd);
    }

    private float HeightByTime(float heightStart, float directionStart, float acceleration, float time)
    {
        return heightStart + (directionStart * time) + (acceleration * Mathf.Pow(time, 2) / 2f);
    }

    private float PeakHeightTime(float directionStart, float acceleration)
    {
        return -(directionStart / acceleration);
    }

    #endregion
}