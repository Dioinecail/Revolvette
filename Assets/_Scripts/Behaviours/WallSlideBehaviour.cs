﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSlideBehaviour : AbstractBehaviour 
{
    public float SlideSpeed;

    private void Update()
    {
        if(collision.OnWall && !collision.OnGround)
        {
            rBody.velocity = Vector2.Lerp(rBody.velocity, new Vector2(rBody.velocity.x, -SlideSpeed), 0.225f);
        }
    }
}
