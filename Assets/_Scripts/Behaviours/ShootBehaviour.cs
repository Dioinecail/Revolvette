﻿using Project.Inputs;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootBehaviour : AbstractBehaviour 
{
    public static event Action<int, int> onAmmoCountChanged;
    public static event Action onReloadStarted;
    public static event Action onReloadFinished;
    public static event Action onShootEvent;

    [SerializeField] private Transform cursorTransform;
    [SerializeField] private Transform GunArmTransform;
    [SerializeField] private Transform GunTransform;
    [SerializeField] private ProjectileBehaviourBase projectileBehaviour;
    [SerializeField] private int maxAmmo = 6;
    [SerializeField] private float reloadDelay = 0.15f;
    [SerializeField] private float shootDelay = 0.2f;

    private int CurrentAmmo;
    private Coroutine coroutine_Reload;
    private Coroutine coroutine_Shoot;
    private bool IsReloading { get => coroutine_Reload != null; }
    private bool CanShoot { get => coroutine_Shoot == null; }
    private Vector2 cursorPosition;



    public void SetNewProjectileBehaviour(ProjectileBehaviourBase behaviour)
    {
        projectileBehaviour = behaviour;
    }

    private void OnShoot()
    {
        if (!CanShoot)
            return;

        if (IsReloading)
        {
            return;
        }
        else if (CurrentAmmo == 0)
        {
            Reload();
            return;
        }

        Vector2 gunPosition = GunTransform.position;
        Vector2 shootDirection = (cursorPosition - gunPosition).normalized;

        projectileBehaviour.Execute(gunPosition, shootDirection, GunTransform.rotation);

        CurrentAmmo--;

        coroutine_Shoot = StartCoroutine(CoroutineShoot());

        onAmmoCountChanged?.Invoke(CurrentAmmo, maxAmmo);
        onShootEvent?.Invoke();
    }

    private void OnAim(Vector2 cursorPosition)
    {
        this.cursorPosition = cursorPosition;
        cursorTransform.position = cursorPosition;

        Vector3 origin = GunArmTransform.position;
        Vector3 direction = origin - (Vector3)cursorPosition;
        Quaternion gunArmRotation = Quaternion.LookRotation(Vector3.forward, direction);

        GunArmTransform.rotation = gunArmRotation;
    }

    private void Reload()
    {
        coroutine_Reload = StartCoroutine(CoroutineReload());
    }

    private void OnEnable()
    {
        CurrentAmmo = maxAmmo;

        InputManager.shootInputEvent += OnShoot;
        InputManager.aimInputEvent += OnAim;
    }

    private void OnDisable()
    {
        InputManager.shootInputEvent -= OnShoot;
        InputManager.aimInputEvent -= OnAim;
    }

    IEnumerator CoroutineReload()
    {
        onReloadStarted?.Invoke();

        int ammoNeeded = maxAmmo - CurrentAmmo;

        for (int a = 0; a < ammoNeeded; a++)
        {
            CurrentAmmo++;

            onAmmoCountChanged?.Invoke(CurrentAmmo, maxAmmo);

            yield return new WaitForSeconds(reloadDelay);
        }

        onReloadFinished?.Invoke();

        coroutine_Reload = null;
	}

    IEnumerator CoroutineShoot()
    {
        yield return new WaitForSeconds(shootDelay);

        coroutine_Shoot = null;
    }
}
