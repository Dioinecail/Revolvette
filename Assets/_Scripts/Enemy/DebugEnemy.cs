﻿namespace Project.Debug
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Project.Enemies;
    using UnityEngine.Events;

    public class DebugEnemy : Enemy
    {
        public UnityEvent onDamageReceived;



        public override void RecieveDamage(int dmg)
        {
            onDamageReceived?.Invoke();
        }
    }
}