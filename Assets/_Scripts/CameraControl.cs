﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class CameraControl : MonoBehaviour 
{
    public float xOffset;
    public float yOffset;
    public float cameraSpeed;

    [Range(0,1)]
    public float middlePointSmoothing;

    private static Transform target;



	public static void SetTarget(Transform tgt)
    {
        target = tgt;
    }

    private void FixedUpdate()
    {
        if(target != null)
        {
            transform.position = Vector3.Lerp(
                transform.position,
                new Vector3(target.position.x, target.position.y + yOffset, transform.position.z),
                cameraSpeed * Time.fixedDeltaTime);
        }
    }
}