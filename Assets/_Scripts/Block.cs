﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Block : MonoBehaviour
{
    [ContextMenu("Reset block")]
    protected virtual void Start()
    {
        int closestX = Mathf.CeilToInt(transform.position.x);
        int closestY = Mathf.CeilToInt(transform.position.y);
		
		if (Mathf.Abs(transform.position.x - Mathf.FloorToInt(transform.position.x)) < (Math.Abs(transform.position.x - closestX)))
		{
			closestX = Mathf.FloorToInt(transform.position.x);
		}

		if (Mathf.Abs(transform.position.y - Mathf.FloorToInt(transform.position.y)) < (Math.Abs(transform.position.y - closestY)))
		{
			closestY = Mathf.FloorToInt(transform.position.y);
		}

		transform.position = new Vector3(closestX, closestY);

    }
    protected virtual void OnDrawGizmos()
    {
		Gizmos.color = Color.yellow;

        int closestX = Mathf.CeilToInt(transform.position.x);
        int closestY = Mathf.CeilToInt(transform.position.y);

		if (Mathf.Abs(transform.position.x - Mathf.FloorToInt(transform.position.x)) < (Math.Abs(transform.position.x - closestX)))
		{
			closestX = Mathf.FloorToInt(transform.position.x);
		}

		if (Mathf.Abs(transform.position.y - Mathf.FloorToInt(transform.position.y)) < (Math.Abs(transform.position.y - closestY)))
		{
			closestY = Mathf.FloorToInt(transform.position.y);
		}

        Gizmos.DrawWireCube(new Vector3(closestX, closestY), Vector2.one);
    }
}