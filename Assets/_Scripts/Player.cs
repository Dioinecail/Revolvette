﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : AbstractBehaviour 
{
    public PlayerStats stats;
	public Image HealthBarMask;

    public bool invulnerable = false;

    public float TrapDamageForce = 5;

    private void Start()
    {
        CameraControl.SetTarget(transform);
        stats.Health = stats.MaxHealth;
        HealthBarMask.fillAmount = (float)stats.Health / stats.MaxHealth;
    }

    public void RecieveDamage(int amount)
    {
        if (invulnerable)
            return;
        rBody.velocity = new Vector2(0, TrapDamageForce);
        StartCoroutine(Invulnerable(1));
        stats.Health -= amount;
        HealthBarMask.fillAmount = (float)stats.Health / stats.MaxHealth;
		StartCoroutine(HealthDecayAnimation());
    }

    void OnDie()
    {
        for (int i = 0; i < GameManager.Instance.Coins; i++)
        {
			Instantiate(BonusManager.Instance.CoinPrefab, transform.position + new Vector3(Random.Range(-.1f, .1f), Random.Range(-.1f, .1f)), Quaternion.identity);
        }
        GameManager.Instance.Coins = 0;
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.collider.tag == "Bonus")
        {
            // Collect bonus depending on it's name
            if(other.gameObject.name.ToLower().Contains("coin"))
            {
                GameManager.Instance.Coins++;
                // Pick up a coin
                Destroy(other.gameObject);
			}

			if (other.gameObject.name.ToLower().Contains("health"))
			{
                stats.Health += 1;
				Destroy(other.gameObject);
			}
        }
        else if (other.collider.tag == "Trap")
        {
            RecieveDamage(1);
        }
    }

	IEnumerator HealthDecayAnimation()
	{
		float targetAngle = 360 - (60 * stats.Health);
		for (int i = 0; i < 10; i++)
		{
			HealthBarMask.transform.localRotation = Quaternion.Lerp(HealthBarMask.transform.localRotation, Quaternion.Euler(0, 0, targetAngle), 0.325f);
			yield return new WaitForEndOfFrame();
		}
	}
    IEnumerator Invulnerable(float time)
    {
        invulnerable = true;
        yield return new WaitForSeconds(time);
        invulnerable = false;
    }
}
