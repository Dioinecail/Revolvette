﻿using UnityEngine;

public delegate void OnReturnToPool(IPoolableObject unit);

public interface IPoolableObject
{
    event OnReturnToPool onReturnToPool;

    void ReturnToPool();
}