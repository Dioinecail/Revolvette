﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {
    Rigidbody2D coinBody;

	void Start () 
    {
        coinBody = GetComponent<Rigidbody2D>();	
	}
	
	void Update () 
    {
        if (coinBody.velocity.magnitude < 0.1f)
        {
            GoToSleep();
        }
    }

    void GoToSleep()
    {
        StartCoroutine(Sleep());
    }

    IEnumerator Sleep()
    {
        yield return new WaitForSeconds(0.5f);

        if (coinBody.velocity.magnitude < 0.1f)
        {
            coinBody.Sleep();
        }
    }
}
