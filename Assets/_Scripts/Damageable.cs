﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour 
{
    int _hp;
    public int HP
    {
        get { return _hp; }
        set
        {
            if(value >= MaxHP)
            {
                _hp = MaxHP;
            }
            else if (value <= 0)
            {
                _hp = 0;
                OnDie();
            }
            else 
            {
                _hp = value;
            }
        }
    }
    public int MaxHP;

    protected virtual void Awake()
    {
        HP = MaxHP;
    }

    public virtual void RecieveDamage(int dmg)
    {
        HP -= dmg;
    }

    protected virtual void OnDie()
    {
        
    }
}
