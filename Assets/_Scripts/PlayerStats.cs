﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void CustomEvent();

[CreateAssetMenu]
public class PlayerStats : ScriptableObject 
{
    public event CustomEvent OnDieEvent;

    int _health;

    public int Health {
        get {return _health;}
        set {
            if(value <= 0) {
                _health = 0;
                if(OnDieEvent != null) 
                    OnDieEvent();
            } else if (value >= MaxHealth) {
                _health = MaxHealth;
            } else {
                _health = value;
            }
        }
    }
    public int MaxHealth = 6;

    public float ReloadTime;
    public int Ammo;
    public int ClipAmmo;
    public float shootRecoil;
    public float ammoSpeed;
    public int Damage;
}
