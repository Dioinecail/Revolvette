﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AmmoUI : MonoBehaviour
{
    // UI
    [SerializeField] private GameObject[] bulletSprites;

    // Animation

    [SerializeField] private RectTransform ammoRoot;
    [SerializeField] private float animationTime = 0.2f;
    [SerializeField] private AnimationCurve rotationCurve;
    [SerializeField] private UnityEvent onClipAnimation;

    private float currentAngle = 330;
    private Coroutine coroutine_ClipAnimation;



    private void OnShoot()
    {
        onClipAnimation?.Invoke();
    }

    private void OnAmmoChanged(int current, int max)
    {
        for (int i = 0; i < bulletSprites.Length; i++)
        {
            bulletSprites[i].SetActive(current > i);
        }

        if (coroutine_ClipAnimation != null)
            StopCoroutine(coroutine_ClipAnimation);

        float fromAngle = currentAngle;
        float toAngle = AngleFromCurrentAmmo(current, max);

        coroutine_ClipAnimation = StartCoroutine(CoroutineClipAnimation(fromAngle, toAngle));
    }

    private float AngleFromCurrentAmmo(int current, int max)
    {
        return -30 + 360f * (float)current / max;
    }

    private IEnumerator CoroutineClipAnimation(float fromAngle, float toAngle)
    {
        float timer = 0;

        while(timer < animationTime)
        {
            timer += Time.deltaTime;

            currentAngle = Mathf.Lerp(fromAngle, toAngle, rotationCurve.Evaluate(timer / animationTime));
            ammoRoot.localRotation = Quaternion.Euler(0, 0, currentAngle);

            yield return null;
        }

        currentAngle = toAngle;
        ammoRoot.localRotation = Quaternion.Euler(0, 0, currentAngle);

        coroutine_ClipAnimation = null;
    }

    private void OnEnable()
    {
        ShootBehaviour.onAmmoCountChanged += OnAmmoChanged;
        ShootBehaviour.onShootEvent += OnShoot;
    }

    private void OnDisable()
    {
        ShootBehaviour.onAmmoCountChanged -= OnAmmoChanged;
        ShootBehaviour.onShootEvent -= OnShoot;
    }
}
