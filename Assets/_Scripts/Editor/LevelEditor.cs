﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Level))]
public class LevelEditor : Editor 
{
    Level level;

    SerializedProperty grids;

    private void OnEnable()
    {
        level = (Level)target;

        grids = serializedObject.FindProperty("grids");
    }
    private void OnSceneGUI()
    {
        EditorGUI.BeginChangeCheck();

        if(level != null && level.grids != null)
        {
			for (int i = 0; i < level.grids.Length; i++)
			{
                Handles.color = Color.yellow;
                Vector2 offset = Handles.FreeMoveHandle(level.grids[i].offset + new Vector2(level.grids[i].sizeX / 2, level.grids[i].sizeY / 2),
                                                               Quaternion.identity,
                                                               Mathf.Min(level.grids[i].sizeY / 2, level.grids[i].sizeX / 2), 
                                                               Vector3.one, 
                                                               Handles.CubeHandleCap) + (new Vector3(-level.grids[i].sizeX / 2, -level.grids[i].sizeY / 2));

                level.grids[i].offset = new Vector2(Mathf.RoundToInt(offset.x), Mathf.RoundToInt(offset.y));

				Handles.color = Color.green;
                Handles.Label(level.grids[i].offset + new Vector2(level.grids[i].sizeX / 2 + 0.75f, level.grids[i].sizeY / 2), "Offset " + i.ToString(), EditorStyles.whiteBoldLabel);
				Handles.Label(level.grids[i].offset + new Vector2(level.grids[i].sizeX + 0.5f, level.grids[i].sizeY + 0.5f), "Size " + i.ToString(), EditorStyles.whiteBoldLabel);


                Handles.Label(level.grids[i].offset + new Vector2(0, level.grids[i].sizeY + 1), "Remove this Grid from array");

                if (Handles.Button(level.grids[i].offset + new Vector2(0, level.grids[i].sizeY), Quaternion.identity, 1, 0.5f, Handles.SphereHandleCap))
                {
                    GridHolder[] newGrids = new GridHolder[level.grids.Length - 1];

                    for (int j = 0; j < newGrids.Length; j++)
                    {
                        if (j < i)
                            newGrids[j] = level.grids[j];
                        else
                            newGrids[j] = level.grids[j + 1];
                    }

                    level.grids = newGrids;
                    return;
                }

				Vector3 topRightHandle = Handles.FreeMoveHandle(level.grids[i].offset + new Vector2(level.grids[i].sizeX, level.grids[i].sizeY), Quaternion.identity, 0.5f, Vector3.one, Handles.CubeHandleCap);
                Vector3 bottomLeftHandle = level.grids[i].offset;

                int X = Mathf.RoundToInt(topRightHandle.x - bottomLeftHandle.x);
                int Y = Mathf.RoundToInt(topRightHandle.y - bottomLeftHandle.y);

                level.grids[i].sizeX = X;
                level.grids[i].sizeY = Y;
			}


            Handles.Label(new Vector3(-4, -2), "Add Grid to array");
            if (Handles.Button(new Vector3(-5, -3), Quaternion.identity, 1, 0.5f, Handles.SphereHandleCap))
			{
                GridHolder[] newGrids = new GridHolder[level.grids.Length + 1];

                for (int i = 0; i < level.grids.Length; i++)
                {
                    newGrids[i] = level.grids[i];
                }

                newGrids[newGrids.Length - 1] = new GridHolder(new Vector2(5, -5), 10, 10);
                level.grids = newGrids;
			}

            Handles.Label(new Vector3(-4, -4), "Remove Grid from array");
            if (Handles.Button(new Vector3(-5, -5), Quaternion.identity, 1, 0.5f, Handles.SphereHandleCap))
			{
				GridHolder[] newGrids = new GridHolder[level.grids.Length - 1];

				for (int i = 0; i < newGrids.Length; i++)
				{
					newGrids[i] = level.grids[i];
				}

                level.grids = newGrids;
            }
        }

        if(EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(target);
            serializedObject.ApplyModifiedProperties();
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    }
}
