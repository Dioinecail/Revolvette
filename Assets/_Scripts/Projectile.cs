﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : PoolableBase 
{
    [SerializeField] private LayerMask collisionMask;
    [SerializeField] private TrailRenderer projectileTrail;

    public float Speed { get; set; }
    public int Damage { get; set; }
    public float CollisionRadius { get; set; }
    public Vector2 InitialDirection { get; set; }
    private Collider2D[] collisionBuffer = new Collider2D[1];



    private void Update() 
    {
        transform.position += (Vector3)InitialDirection * Speed * Time.deltaTime;
    }

    private void LateUpdate()
    {
        collisionBuffer[0] = null;

        Physics2D.OverlapCircleNonAlloc(transform.position, CollisionRadius, collisionBuffer, collisionMask);

        if (collisionBuffer[0] != null)
        {
            Damageable obj = collisionBuffer[0].GetComponent<Damageable>();

            if(obj != null)
            {
                obj.RecieveDamage(Damage);
            }
                ReturnToPool();
        }
    }

    public override void ReturnToPool()
    {
        base.ReturnToPool();

        projectileTrail.Clear();
    }
}